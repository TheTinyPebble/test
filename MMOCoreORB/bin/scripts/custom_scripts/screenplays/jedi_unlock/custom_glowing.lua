local ObjectManager = require("managers.object.object_manager")
local Logger = require("utils.logger")

CustomGlowing = ScreenPlay:new {
	requiredBadges = {
		{ type = "exploration_jedi", amount = 3 },
		{ type = "exploration_dangerous", amount = 2 },
		{ type = "exploration_easy", amount = 5 },
		{ type = "master", amount = 1 },
		{ type = "content", amount = 5 },
	}
}

function CustomGlowing:getCompletedBadgeTypeCount(pPlayer)
	local pGhost = CreatureObject(pPlayer):getPlayerObject()

	if (pGhost == nil) then
		return 0
	end

	local typesCompleted = 0

	for i = 1, #self.requiredBadges, 1 do
		local type = self.requiredBadges[i].type
		local requiredAmount = self.requiredBadges[i].amount

		local badgeListByType = getBadgeListByType(type)
		local badgeCount = 0

		for j = 1, #badgeListByType, 1 do
			if PlayerObject(pGhost):hasBadge(badgeListByType[j]) then
				badgeCount = badgeCount + 1
			end
		end

		if badgeCount >= requiredAmount then
			typesCompleted = typesCompleted + 1
		end
	end

	return typesCompleted
end

function CustomGlowing:hasRequiredBadgeCount(pPlayer)
	return self:getCompletedBadgeTypeCount(pPlayer) == #self.requiredBadges
end

-- Check if the player is CustomGlowing or not.
-- @param pPlayer pointer to the creature object of the player.
function CustomGlowing:isGlowing(pPlayer)
	return CustomJediManagerCommon.hasJediProgressionScreenPlayState(pPlayer, CUSTOM_JEDI_PROGRESSION_GLOWING)
end

-- Event handler for the BADGEAWARDED event.
-- @param pPlayer pointer to the creature object of the player who was awarded with a badge.
-- @param pPlayer2 pointer to the creature object of the player who was awarded with a badge.
-- @param badgeNumber the badge number that was awarded.
-- @return 0 to keep the observer active.
function CustomGlowing:badgeAwardedEventHandler(pPlayer, pPlayer2, badgeNumber)
	if (pPlayer == nil) then
		return 0
	end

	if self:hasRequiredBadgeCount(pPlayer) and not CustomJediManagerCommon.hasJediProgressionScreenPlayState(pPlayer, CUSTOM_JEDI_PROGRESSION_GLOWING) then
		CustomJediManagerCommon.setJediProgressionScreenPlayState(pPlayer, CUSTOM_JEDI_PROGRESSION_GLOWING)
		return 1
	end

	return 0
end

-- Register observer on the player for observing badge awards.
-- @param pPlayer pointer to the creature object of the player to register observers on.
function CustomGlowing:registerObservers(pPlayer)
	dropObserver(BADGEAWARDED, "CustomGlowing", "badgeAwardedEventHandler", pPlayer)
	createObserver(BADGEAWARDED, "CustomGlowing", "badgeAwardedEventHandler", pPlayer)
end

-- Handling of the onPlayerLoggedIn event. The progression of the player will be checked and observers will be registered.
-- @param pPlayer pointer to the creature object of the player who logged in.
function CustomGlowing:onPlayerLoggedIn(pPlayer)
	if not self:isGlowing(pPlayer) then
		if self:hasRequiredBadgeCount(pPlayer) then
			CustomJediManagerCommon.setJediProgressionScreenPlayState(pPlayer, CUSTOM_JEDI_PROGRESSION_GLOWING)
		else
			self:registerObservers(pPlayer)
		end
	end
end

-- Handling of the checkForceStatus command.
-- @param pPlayer pointer to the creature object of the player who performed the command
function CustomGlowing:checkForceStatusCommand(pPlayer)
	local progress = "@jedi_spam:fs_progress_" .. self:getCompletedBadgeTypeCount(pPlayer)

	CreatureObject(pPlayer):sendSystemMessage(progress)
end

return CustomGlowing
