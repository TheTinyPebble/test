local ObjectManager = require("managers.object.object_manager")
local QuestManager = require("managers.quest.quest_manager")

CustomJediManagerCommon = ScreenPlay:new {
}

CUSTOM_JEDI_PROGRESSION_SCREEN_PLAY_STATE_STRING = "CustomJediProgression"
CUSTOM_JEDI_PROGRESSION_GLOWING = 1
CUSTOM_JEDI_PROGRESSION_HAS_CRYSTAL = 2
CUSTOM_JEDI_PROGRESSION_ACCEPTED_MELLICHAE = 4
CUSTOM_JEDI_PROGRESSION_DEFATED_MELLICHAE = 8
CUSTOM_JEDI_PROGRESSION_COMPLETED_FIRST_HOLOCRON = 16
CUSTOM_JEDI_PROGRESSION_COMPLETED_SECOND_HOLOCRON = 32
CUSTOM_JEDI_PROGRESSION_COMPLETED_THIRD_HOLOCRON = 64
CUSTOM_JEDI_PROGRESSION_COMPLETED_FOURTH_HOLOCRON = 128
CUSTOM_JEDI_PROGRESSION_COMPLETED_FIFTH_HOLOCRON = 256
CUSTOM_JEDI_PROGRESSION_COMPLETED_TRIALS = 512

-- Set the jedi progression screen play state on the player.
-- @param pPlayer pointer to the creature object of the player.
-- @param state the state to set.
function CustomJediManagerCommon.setJediProgressionScreenPlayState(pPlayer, state)
	if (pPlayer == nil) then
		return
	end

	CreatureObject(pPlayer):setScreenPlayState(state, CUSTOM_JEDI_PROGRESSION_SCREEN_PLAY_STATE_STRING)
end

function CustomJediManagerCommon.isHolocronEligible(pPlayer)
	if (pPlayer == nil) then
		return false
	end

	return CustomJediManagerCommon.hasJediProgressionScreenPlayState(pPlayer, CUSTOM_JEDI_PROGRESSION_DEFATED_MELLICHAE)
end

-- Check if the player has the jedi progression screen play state.
-- @param pPlayer pointer to the creature object of the player.
-- @param state the state to check if the player has.
-- @return true if the player has the state.
function CustomJediManagerCommon.hasJediProgressionScreenPlayState(pPlayer, state)
	if (pPlayer == nil) then
		return false
	end

	return CreatureObject(pPlayer):hasScreenPlayState(state, CUSTOM_JEDI_PROGRESSION_SCREEN_PLAY_STATE_STRING)
end

return CustomJediManagerCommon
