--Prologue
includeFile("../custom_scripts/screenplays/wod/prologue/mysteriousWitchGoTo.lua")
	--Conversation Handlers
includeFile("../custom_scripts/screenplays/wod/prologue/convo_handlers/wod_omoggs_rep_conv_handler.lua")
	--Walkabout
includeFile("../custom_scripts/screenplays/wod/prologue/walkabout/walkaboutGoTo1.lua")
includeFile("../custom_scripts/screenplays/wod/prologue/walkabout/walkaboutGoTo2.lua")
includeFile("../custom_scripts/screenplays/wod/prologue/walkabout/walkaboutGoTo3.lua")
includeFile("../custom_scripts/screenplays/wod/prologue/walkabout/walkaboutGoTo4.lua")
includeFile("../custom_scripts/screenplays/wod/prologue/walkabout/walkaboutGoTo5.lua")
includeFile("../custom_scripts/screenplays/wod/prologue/walkabout/walkaboutGoTo6.lua")
includeFile("../custom_scripts/screenplays/wod/prologue/walkabout/walkaboutGoTo7.lua")
includeFile("../custom_scripts/screenplays/wod/prologue/walkabout/walkaboutGoTo8.lua")
