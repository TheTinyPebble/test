local ObjectManager = require("managers.object.object_manager")
local QuestManager = require("managers.quest.quest_manager")
require("utils.helpers")

walkaboutGoTo2 = GoToLocation:new {
	-- Task properties
	taskName = "walkaboutGoTo2",
	-- GoToLocation properties
	waypointDescription = "@theme_park/wod/prologue/wod_prologue_walkabout:task02_waypoint_name",
	spawnPoint = { x = 4747, y = 2399 },
	spawnPlanet = "dathomir",
	spawnRadius = 4,
}

-- Event handler for the enter active area event.
-- The event will complete the task.
-- @param pPlayer pointer to the creature object of the player.
function walkaboutGoTo2:onEnteredActiveArea(pPlayer)
	if (pPlayer == nil) then
		return 1
	end

	
	local suiManager = LuaSuiManager()
	local sui = SuiMessageBox.new("walkaboutGoTo2", "notifyOkPressed")
	sui.setTitle("@theme_park/wod/prologue/wod_prologue_walkabout:task_comm_message_title_02")
	sui.setPrompt("@theme_park/wod/prologue/wod_prologue_walkabout:task02_comm_message_text_02")
	sui.setOkButtonText("@ok")
	sui.hideCancelButton()
	sui.sendTo(pPlayer)
	return 1
end

function walkaboutGoTo2:notifyOkPressed(pPlayer)
	if (pPlayer == nil) then
		return 1
	end
	
	QuestManager.completeQuest(pPlayer, QuestManager.quests.WOD_PROLOGUE_WALKABOUT_02)
	self:finish(pPlayer)
	walkaboutGoTo3:start(pPlayer)
end

-- Event handler for the onSuccessfulSpawn.
-- The event will activate the quest.
-- @param pPlayer pointer to the creature object of the player.
function walkaboutGoTo2:onSuccessfulSpawn(pPlayer)
	if (pPlayer == nil) then
		return
	end

	QuestManager.activateQuest(pPlayer, QuestManager.quests.WOD_PROLOGUE_WALKABOUT_02)
end

return walkaboutGoTo2
