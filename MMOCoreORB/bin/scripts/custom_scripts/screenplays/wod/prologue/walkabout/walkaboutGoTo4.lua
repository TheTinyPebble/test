local ObjectManager = require("managers.object.object_manager")
local QuestManager = require("managers.quest.quest_manager")
require("utils.helpers")

walkaboutGoTo4 = GoToLocation:new {
	-- Task properties
	taskName = "walkaboutGoTo4",
	-- GoToLocation properties
	waypointDescription = "@theme_park/wod/prologue/wod_prologue_walkabout:task04_waypoint_name",
	spawnPoint = { x = 552, y = 3074  },
	spawnPlanet = "dathomir",
	spawnRadius = 4,
}

-- Event handler for the enter active area event.
-- The event will complete the task.
-- @param pPlayer pointer to the creature object of the player.
function walkaboutGoTo4:onEnteredActiveArea(pPlayer)
	if (pPlayer == nil) then
		return 1
	end

	return 1
end

-- Event handler for the onSuccessfulSpawn.
-- The event will activate the quest.
-- @param pPlayer pointer to the creature object of the player.
function walkaboutGoTo4:onSuccessfulSpawn(pPlayer)
	if (pPlayer == nil) then
		return
	end

	QuestManager.activateQuest(pPlayer, QuestManager.quests.WOD_PROLOGUE_WALKABOUT_04)
end

return walkaboutGoTo4
