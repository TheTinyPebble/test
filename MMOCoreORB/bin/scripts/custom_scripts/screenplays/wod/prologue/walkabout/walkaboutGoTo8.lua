local ObjectManager = require("managers.object.object_manager")
local QuestManager = require("managers.quest.quest_manager")
require("utils.helpers")

walkaboutGoTo8 = GoToLocation:new {
	-- Task properties
	taskName = "walkaboutGoTo8",
	-- GoToLocation properties
	waypointDescription = "@theme_park/wod/prologue/wod_prologue_walkabout:task08_waypoint_name",
	spawnPoint = { x = 552, y = 3074  },
	spawnPlanet = "dathomir",
	spawnRadius = 4,
}

-- Event handler for the enter active area event.
-- The event will complete the task.
-- @param pPlayer pointer to the creature object of the player.
function walkaboutGoTo8:onEnteredActiveArea(pPlayer)
	if (pPlayer == nil) then
		return 1
	end

	return 1
end

function walkaboutGoTo8:meetMysteriousWitchPrompt(pPlayer)
	if (pPlayer == nil) then
		return 1
	end
	
	local suiManager = LuaSuiManager()
	local sui = SuiMessageBox.new("walkaboutGoTo8", "notifyOkPressed")
	sui.setTitle("@theme_park/wod/prologue/wod_prologue_walkabout:task_comm_message_title_02")
	sui.setPrompt("@theme_park/wod/prologue/wod_prologue_walkabout:task08_comm_message_text_02")
	sui.setOkButtonText("@ok")
	sui.hideCancelButton()
	sui.sendTo(pPlayer)
end

function walkaboutGoTo8:notifyOkPressed(pPlayer) 
	if (pPlayer == nil) then
		return 1
	end
	
	mysteriousWitchGoTo:start(pPlayer)
end

-- Event handler for the onSuccessfulSpawn.
-- The event will activate the quest.
-- @param pPlayer pointer to the creature object of the player.
function walkaboutGoTo8:onSuccessfulSpawn(pPlayer)
	if (pPlayer == nil) then
		return
	end

	QuestManager.activateQuest(pPlayer, QuestManager.quests.WOD_PROLOGUE_WALKABOUT_08)
end

return walkaboutGoTo8
