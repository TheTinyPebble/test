local ObjectManager = require("managers.object.object_manager")
local QuestManager = require("managers.quest.quest_manager")
require("utils.helpers")

walkaboutGoTo3 = GoToLocation:new {
	-- Task properties
	taskName = "walkaboutGoTo3",
	-- GoToLocation properties
	waypointDescription = "@theme_park/wod/prologue/wod_prologue_walkabout:task03_waypoint_name",
	spawnPoint = { x = 5702, y = 1937 },
	spawnPlanet = "dathomir",
	spawnRadius = 4,
}

-- Event handler for the enter active area event.
-- The event will complete the task.
-- @param pPlayer pointer to the creature object of the player.
function walkaboutGoTo3:onEnteredActiveArea(pPlayer)
	if (pPlayer == nil) then
		return 1
	end

	local suiManager = LuaSuiManager()
	local sui = SuiMessageBox.new("walkaboutGoTo3", "notifyOkPressed")
	sui.setTitle("@theme_park/wod/prologue/wod_prologue_walkabout:task_comm_message_title_02")
	sui.setPrompt("@theme_park/wod/prologue/wod_prologue_walkabout:task03_comm_message_text_02")
	sui.setOkButtonText("@ok")
	sui.hideCancelButton()
	sui.sendTo(pPlayer)
	return 1
end

function walkaboutGoTo3:notifyOkPressed(pPlayer)
	if (pPlayer == nil) then
		return 1
	end
	
	QuestManager.completeQuest(pPlayer, QuestManager.quests.WOD_PROLOGUE_WALKABOUT_03)
	self:finish(pPlayer)
	walkaboutGoTo4:start(pPlayer)
end

-- Event handler for the onSuccessfulSpawn.
-- The event will activate the quest.
-- @param pPlayer pointer to the creature object of the player.
function walkaboutGoTo3:onSuccessfulSpawn(pPlayer)
	if (pPlayer == nil) then
		return
	end

	QuestManager.activateQuest(pPlayer, QuestManager.quests.WOD_PROLOGUE_WALKABOUT_03)
end

return walkaboutGoTo3
