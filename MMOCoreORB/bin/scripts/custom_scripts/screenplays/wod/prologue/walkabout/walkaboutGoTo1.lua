local ObjectManager = require("managers.object.object_manager")
local QuestManager = require("managers.quest.quest_manager")
require("utils.helpers")

walkaboutGoTo1 = GoToLocation:new {
	-- Task properties
	taskName = "walkaboutGoTo1",
	-- GoToLocation properties
	waypointDescription = "@theme_park/wod/prologue/wod_prologue_walkabout:task01_waypoint_name",
	spawnPoint = { x = 3055, y = 1366 }, 
	spawnPlanet = "dathomir",
	spawnRadius = 4,
}

-- Event handler for the enter active area event.
-- The event will complete the task.
-- @param pPlayer pointer to the creature object of the player.
function walkaboutGoTo1:onEnteredActiveArea(pPlayer)
	if (pPlayer == nil) then
		return 1
	end

	local sui = SuiMessageBox.new("walkaboutGoTo1", "notifyOkPressed01")
	sui.setTitle("@theme_park/wod/prologue/wod_prologue_walkabout:task_comm_message_title_01")
	sui.setPrompt("@theme_park/wod/prologue/wod_prologue_walkabout:task01_comm_message_text_01")
	sui.setOkButtonText("@ok")
	sui.hideCancelButton()
	sui.sendTo(pPlayer)
	return 1
end

function walkaboutGoTo1:notifyOkPressed01(pPlayer)
	if (pPlayer == nil) then
		return 1
	end
	
	QuestManager.completeQuest(pPlayer, QuestManager.quests.WOD_PROLOGUE_WALKABOUT_01)
	self:finish(pPlayer)
	walkaboutGoTo2:start(pPlayer)
	createEvent(5000, "walkaboutGoTo1", "sendSecondMessage", pPlayer, "")
end

function walkaboutGoTo1:sendSecondMessage(pPlayer)
	if (pPlayer == nil) then
		return 1
	end
	local sui = SuiMessageBox.new("walkaboutGoTo1", "notifyOkPressed02")
	sui.setTitle("@theme_park/wod/prologue/wod_prologue_walkabout:task_comm_message_title_02")
	sui.setPrompt("@theme_park/wod/prologue/wod_prologue_walkabout:task01_comm_message_text_02")
	sui.setOkButtonText("@ok")
	sui.hideCancelButton()
	sui.sendTo(pPlayer)
	return 1
end

function walkaboutGoTo1:notifyOkPressed02(pPlayer)
	if (pPlayer == nil) then
		return 1
	end
end


-- Event handler for the onSuccessfulSpawn.
-- The event will activate the quest.
-- @param pPlayer pointer to the creature object of the player.
function walkaboutGoTo1:onSuccessfulSpawn(pPlayer)
	if (pPlayer == nil) then
		return
	end
	
	QuestManager.activateQuest(pPlayer, QuestManager.quests.WOD_PROLOGUE_WALKABOUT_MAIN)
	QuestManager.activateQuest(pPlayer, QuestManager.quests.WOD_PROLOGUE_WALKABOUT_01)
end

return walkaboutGoTo1
