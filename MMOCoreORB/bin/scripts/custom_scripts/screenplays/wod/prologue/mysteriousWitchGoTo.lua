local ObjectManager = require("managers.object.object_manager")
local QuestManager = require("managers.quest.quest_manager")
require("utils.helpers")

mysteriousWitchGoTo = GoToLocation:new {
	-- Task properties
	taskName = "mysteriousWitchGoTo",
	-- GoToLocation properties
	waypointDescription = "@quest/ground/wod_prologue_walkabout_02:task10_waypoint_name", -- Temporary
	spawnPoint = { x = -3177, y = 3184  },
	spawnPlanet = "dathomir",
	spawnRadius = 4,
}

-- Event handler for the enter active area event.
-- The event will complete the task.
-- @param pPlayer pointer to the creature object of the player.
function mysteriousWitchGoTo:onEnteredActiveArea(pPlayer)
	if (pPlayer == nil) then
		return 1
	end

	return 1
end

-- Event handler for the onSuccessfulSpawn.
-- The event will activate the quest.
-- @param pPlayer pointer to the creature object of the player.
function mysteriousWitchGoTo:onSuccessfulSpawn(pPlayer)
	if (pPlayer == nil) then
		return
	end

	QuestManager.activateQuest(pPlayer, QuestManager.quests.WOD_PROLOGUE_MEET_MYSTERIOUS_WITCH)
end

return mysteriousWitchGoTo
