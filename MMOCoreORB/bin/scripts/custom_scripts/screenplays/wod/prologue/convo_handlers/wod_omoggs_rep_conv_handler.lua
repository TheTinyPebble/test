local ObjectManager = require("managers.object.object_manager")
local QuestManager = require("managers.quest.quest_manager")

wodOmoggsRepConvoHandler = conv_handler:new {}

function wodOmoggsRepConvoHandler:getInitialScreen(pPlayer, pNpc, pConvTemplate)
	local convoTemplate = LuaConversationTemplate(pConvTemplate)
	
	if (QuestManager.hasActiveQuest(pPlayer, QuestManager.quests.WOD_PROLOGUE_WALKABOUT_04)) then
		return convoTemplate:getScreen("intro_complete_first_phase")
	elseif (QuestManager.hasActiveQuest(pPlayer, QuestManager.quests.WOD_PROLOGUE_WALKABOUT_08)) then
		return convoTemplate:getScreen("intro_complete_second_phase")
	end
	
	if (QuestManager.hasCompletedQuest(pPlayer, QuestManager.quests.WOD_PROLOGUE_WALKABOUT_04) and not QuestManager.hasActiveQuest(pPlayer, QuestManager.quests.WOD_PROLOGUE_WALKABOUT_05)) then
		return convoTemplate:getScreen("intro_start_second_phase")
	end
	
	if (QuestManager.hasCompletedQuest(pPlayer, QuestManager.quests.WOD_PROLOGUE_WALKABOUT_MAIN)) then
		return convoTemplate:getScreen("intro_quest_complete")
	end
	
	if (QuestManager.hasActiveQuest(pPlayer, QuestManager.quests.WOD_PROLOGUE_WALKABOUT_MAIN)) then
		wodOmoggsRepConvoHandler:resetQuest(pPlayer)
		return convoTemplate:getScreen("intro_in_progress")
	else
		return convoTemplate:getScreen("intro")
	end
end

function wodOmoggsRepConvoHandler:runScreenHandlers(pConvTemplate, pPlayer, pNpc, selectedOption, pConvScreen)
	local screen = LuaConversationScreen(pConvScreen)
	local screenID = screen:getScreenID()

	if (screenID == "start_first_phase") then
		walkaboutGoTo1:start(pPlayer)
	elseif (screenID == "complete_first_phase") then
		walkaboutGoTo4:finish(pPlayer)
		QuestManager.completeQuest(pPlayer, QuestManager.quests.WOD_PROLOGUE_WALKABOUT_04)
	elseif (screenID == "start_second_phase") then
		walkaboutGoTo5:start(pPlayer)
	elseif (screenID == "complete_second_phase") then
		walkaboutGoTo8:finish(pPlayer)
		QuestManager.completeQuest(pPlayer, QuestManager.quests.WOD_PROLOGUE_WALKABOUT_08)
		QuestManager.completeQuest(pPlayer, QuestManager.quests.WOD_PROLOGUE_WALKABOUT_09)
		QuestManager.completeQuest(pPlayer, QuestManager.quests.WOD_PROLOGUE_WALKABOUT_MAIN)
		createEvent(5000, "walkaboutGoTo8", "meetMysteriousWitchPrompt", pPlayer, "") -- Launches the next part of the themepark.
	end

	return pConvScreen
end

function wodOmoggsRepConvoHandler:resetQuest(pPlayer)
	if (pPlayer == nil) then
		return
	end
	
	for i = 1, 8, 1 do
		local questID

		questID = getPlayerQuestID("wod_prologue_walkabout_0" .. i)
		
		if QuestManager.hasActiveQuest(pPlayer, questID) then
			createEvent(1000, "walkaboutGoTo" .. i, "finish", pPlayer, "")
			createEvent(1000, "walkaboutGoTo" .. i, "start", pPlayer, "")
		end
	end
end
