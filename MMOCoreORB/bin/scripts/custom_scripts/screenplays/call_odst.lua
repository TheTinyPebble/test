callODST = ScreenPlay:new {
	testOptions = {
		{faction = "CIS", level = 1, mobile = "cis_battle_droid", mobileCount = 4},
		{faction = "CIS", level = 2, mobile = "cis_sbd", mobileCount = 2},
		{faction = "CIS", level = 3, mobile = "cis_magnaguard", mobileCount = 1},
		{faction = "Republic", level = 1, mobile = "clonetrooper", mobileCount = 4},
		{faction = "Republic", level = 2, mobile = "clone_commando", mobileCount = 2},
		{faction = "Republic", level = 3, mobile = "rep_jedi_knight", mobileCount = 1}
	}
}

registerScreenPlay("callODST", false)

local ObjectManager = require("managers.object.object_manager")

function callODST:showMainUI(pPlayer)
	local sui = SuiListBox.new("callODST", "mainCallBack")

	sui.setTargetNetworkId(SceneObject(pPlayer):getObjectID())

	local suiBody = "Select the faction and level of orbital drop shock troopers you wish to call."
	sui.setTitle("Call ODST Menu")
	sui.setPrompt(suiBody)
	
	for i = 1, #self.testOptions do
		sui.add("Call level " .. self.testOptions[i].level .. " " .. self.testOptions[i].faction .. " ODST", "")
	end

	sui.sendTo(pPlayer)
end

function callODST:mainCallBack(pPlayer, pSui, eventIndex, args)
	local cancelPressed = (eventIndex == 1)

	if (cancelPressed or args == nil or tonumber(args) < 0) then
		return
	end
	
	local selection = args + 1
	
	CreatureObject(pPlayer):sendSystemMessage("Placing beacon, ODSTs will be here in 30 seconds.")
	
	local planet = SceneObject(pPlayer):getZoneName()
	local x = SceneObject(pPlayer):getWorldPositionX()
	local z = SceneObject(pPlayer):getWorldPositionZ()
	local y = SceneObject(pPlayer):getWorldPositionY()
	local pBeacon = spawnSceneObject(planet, "object/static/item/item_tool_sensor_beacon.iff", x + 1, z, y, 0, 0)
	createEvent(30 * 1000, "callODST", "dropODST", pBeacon, tostring(selection))
end

function callODST:dropODST(pBeacon, selection)
	if (pBeacon == nil) then
		return
	end
	
	local planet = SceneObject(pBeacon):getZoneName()
	local x = SceneObject(pBeacon):getWorldPositionX()
	local z = SceneObject(pBeacon):getWorldPositionZ()
	local y = SceneObject(pBeacon):getWorldPositionY()
	
	SceneObject(pBeacon):playEffect("clienteffect/combat_explosion_lair_large.cef", "")
	local pPod = spawnSceneObject(planet, "object/static/structure/general/escape_pod.iff", x, z, y, 0, 0)
	
	local spawnCoords = self:generateCircleSpawn(self.testOptions[tonumber(selection)].mobileCount, 4, pPod)
	for i,coord in pairs(spawnCoords) do
		spawnMobile(planet, self.testOptions[tonumber(selection)].mobile, 0, coord.x, self.z, coord.y, getRandomNumber(0, 360), 0)
	end

	createEvent(1000, "callODST", "destroyObject", pBeacon, "")
	createEvent(5 * 60 * 1000, "callODST", "destroyObject", pPod, "")
end

-- p = # of mobs. r = radius of circle
function callODST:generateCircleSpawn(p, r, pPod)
	local slice = (2 * math.pi / p)
	local angle, newX, newY
	local spawnCoords = {}
	for i = 1, p, 1 do
		angle = slice * i
		newX = (SceneObject(pPod):getWorldPositionX() + r * math.cos(angle))
		newY = (SceneObject(pPod):getWorldPositionY()+ r * math.sin(angle))
		table.insert(spawnCoords, { x = newX, y = newY })
	end
	return spawnCoords
end

function callODST:destroyObject(pObject)
	if (pObject == nil) then
		return
	end
	
	SceneObject(pObject):destroyObjectFromWorld()
end
