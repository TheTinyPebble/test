--Sub Folders
includeFile("../custom_scripts/screenplays/wod/screenplays.lua")
includeFile("../custom_scripts/screenplays/convo_handlers/screenplays.lua")
includeFile("../custom_scripts/screenplays/jedi_unlock/screenplays.lua")

--Child Objects
includeFile("../custom_scripts/screenplays/glowing.lua")
includeFile("../custom_scripts/screenplays/dungeon/dath_bunker/dath_bunker.lua")
--includeFile("../custom_scripts/screenplays/deathBounty.lua")
includeFile("../custom_scripts/screenplays/eliteSpawns.lua")
includeFile("../custom_scripts/screenplays/eliteSpawnMap.lua")
includeFile("../custom_scripts/screenplays/jawaRescueOperation.lua")
includeFile("../custom_scripts/screenplays/call_odst.lua")
