jawaRescueOperation = ScreenPlay:new {
	scriptName = "jawaRescueOperation",
	planet = "tatooine",
}

registerScreenPlay("jawaRescueOperation", true)

local ObjectManager = require("managers.object.object_manager")

function jawaRescueOperation:start()
	if (isZoneEnabled(self.planet)) then
		self:spawnMobiles()
	end
end

function jawaRescueOperation:spawnMobiles()
	local pMobile = spawnMobile(self.planet, "jawa_rescue_distressed_jawa", -1, -3105.8, 5, 2183.4, -112, 0) --Temporary Mobile and location, items below will be set in the actual mobile
	self:setMoodString(pMobile, "worried")
	
	local pEscort = spawnMobile(self.planet, "jawa_rescue_jawa_captain", -1, -3103.8, 5, 2182.4, -112, 0) --Temporary Mobile and location, items below will be set in the actual mobile
end

function jawaRescueOperation:handleReward(pPlayer)
	CreatureObject(pPlayer):sendSystemMessage("Temporary Message: Imagine this is a nice sandcrawler blueprint for you to make.")
end
