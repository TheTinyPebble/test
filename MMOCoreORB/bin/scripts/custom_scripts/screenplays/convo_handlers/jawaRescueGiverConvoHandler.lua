local ObjectManager = require("managers.object.object_manager")
local QuestManager = require("managers.quest.quest_manager")

jawaRescueGiverConvoHandler = conv_handler:new {}

function jawaRescueGiverConvoHandler:getInitialScreen(pPlayer, pNpc, pConvTemplate)
	local convoTemplate = LuaConversationTemplate(pConvTemplate)
	
	if (QuestManager.hasActiveQuest(pPlayer, QuestManager.quests.JAWA_RESCUE_OP_TASK)) then
		return convoTemplate:getScreen("quest_in_progress")
	end
	
	if (QuestManager.hasActiveQuest(pPlayer, QuestManager.quests.JAWA_RESCUE_OP_RETURN)) then
		return convoTemplate:getScreen("return_quest")
	end
	
	if (QuestManager.hasCompletedQuest(pPlayer, QuestManager.quests.JAWA_RESCUE_OP)) then
		return convoTemplate:getScreen("quest_completed")
	end
	
	return convoTemplate:getScreen("init")
end

function jawaRescueGiverConvoHandler:runScreenHandlers(pConvTemplate, pPlayer, pNpc, selectedOption, pConvScreen)
	local screen = LuaConversationScreen(pConvScreen)
	local screenID = screen:getScreenID()
	local pConvScreen = screen:cloneScreen()
	local clonedConversation = LuaConversationScreen(pConvScreen)
	
	if (screenID == "init") then
		if (not CreatureObject(pPlayer):hasSkill("combat_smuggler_underworld_01")) then
			clonedConversation:setCustomDialogText("Ahhh! Padha njupuk, padha njupuk kabeh mau! /n/n[You can tell the Jawa is distressed.]")
		end
	end
	
	if (screenID == "information_one") then
		if (not CreatureObject(pPlayer):hasSkill("combat_smuggler_underworld_01")) then
			clonedConversation:setCustomDialogText("Ing Tuskens, dheweke diculik kabeh kru lan kaptenku! Apa aku arep nggawe? /n/n[From the gestures of the Jawa you guess someone has been kidnapped.]")
		end
	end
	
	if (screenID == "information_two") then
		if (not CreatureObject(pPlayer):hasSkill("combat_smuggler_underworld_01")) then
			clonedConversation:setCustomDialogText("*Deep breath* Aku melu menyang bunker lawas, aku wis ngarep-arep bisa teka karo rencana kanggo ngluwari, nanging ana akeh banget Tuskens. /n/n[After much back and forth the Jawa realizes he can upload the location to your datapad.]")
		end
	end
	
	if (screenID == "information_three") then
		if (not CreatureObject(pPlayer):hasSkill("combat_smuggler_underworld_01")) then
			clonedConversation:setCustomDialogText("Aku ora ngerti, nanging ana akeh! Nalika aku teka, kanca-kancaku padha kerja keras, katon kaya turu. /n/n[From the gestures you can tell that there is a lot of Tuskens at the location.]")
		end
	end
	
	if (screenID == "start_quest") then
		if (not CreatureObject(pPlayer):hasSkill("combat_smuggler_underworld_01")) then
			clonedConversation:setCustomDialogText("Sampeyan bakal? Matur nuwun gurun! /n/n[The Jawa looks appreciative.]")
		end
		QuestManager.activateQuest(pPlayer, QuestManager.quests.JAWA_RESCUE_OP)
		QuestManager.activateQuest(pPlayer, QuestManager.quests.JAWA_RESCUE_OP_TASK)
	end
	
	if (screenID == "return_quest") then
		if (not CreatureObject(pPlayer):hasSkill("combat_smuggler_underworld_01")) then
			clonedConversation:setCustomDialogText("Kapten rawuh ing kene ora kepungkur lan nyeritakake kabeh sing dilakoni Tuskens. /n/n[The Jawa looks like he's settled down a bit, you guess the captain has already arrived to explain what happened.]")
		end
	end
	
	if (screenID == "quest_in_progress") then
		if (not CreatureObject(pPlayer):hasSkill("combat_smuggler_underworld_01")) then
			clonedConversation:setCustomDialogText("Mangga cepet-cepet munggah lan bantuan kru! /n/n[The Jawa looks at you with pleading eyes.]")
		end
	end
	
	if (screenID == "quest_completed") then
		if (not CreatureObject(pPlayer):hasSkill("combat_smuggler_underworld_01")) then
			clonedConversation:setCustomDialogText("Matur nuwun kanggo kabeh pitulung! /n/n[The Jawa looks grateful.]")
		end
	end
	
	if (screenID == "complete_quest") then
		if (not CreatureObject(pPlayer):hasSkill("combat_smuggler_underworld_01")) then
			clonedConversation:setCustomDialogText("Aku matur nuwun kanggo pitulunganmu, minangka thanks aku bakal menehi sampeyan cithak cetak kanggo sandcrawler. /n/n[The Jawa looks grateful and hands over a blueprint, it looks to be of a sandcrawler.]")
		end
		QuestManager.completeQuest(pPlayer, QuestManager.quests.JAWA_RESCUE_OP)
		QuestManager.completeQuest(pPlayer, QuestManager.quests.JAWA_RESCUE_OP_RETURN)
		jawaRescueOperation:handleReward(pPlayer)
	end
	
	if (screenID == "decline") then
		if (not CreatureObject(pPlayer):hasSkill("combat_smuggler_underworld_01")) then
			clonedConversation:setCustomDialogText("Ahhhh! Aku ora bisa nulungi wong liya! /n/n[The Jawa looks distressed.]")
		end
	end
	return pConvScreen
end
