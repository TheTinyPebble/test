local ObjectManager = require("managers.object.object_manager")
local QuestManager = require("managers.quest.quest_manager")

jawaRescueCaptainConvoHandler = conv_handler:new {}

function jawaRescueCaptainConvoHandler:getInitialScreen(pPlayer, pNpc, pConvTemplate)
	local convoTemplate = LuaConversationTemplate(pConvTemplate)
	
	if (QuestManager.hasActiveQuest(pPlayer, QuestManager.quests.JAWA_RESCUE_OP_TASK)) then
		return convoTemplate:getScreen("quest")
	end

	if (QuestManager.hasCompletedQuest(pPlayer, QuestManager.quests.JAWA_RESCUE_OP)) then
		return convoTemplate:getScreen("quest_completed")
	end
	
	return convoTemplate:getScreen("reject")
end

function jawaRescueCaptainConvoHandler:runScreenHandlers(pConvTemplate, pPlayer, pNpc, selectedOption, pConvScreen)
	local screen = LuaConversationScreen(pConvScreen)
	local screenID = screen:getScreenID()
	local pConvScreen = screen:cloneScreen()
	local clonedConversation = LuaConversationScreen(pConvScreen)

	if (screenID == "quest") then
		if (not CreatureObject(pPlayer):hasSkill("combat_smuggler_underworld_01")) then
			clonedConversation:setCustomDialogText("Mangga nyuwun pangapunten! /n/n[The Jawa looks at you pleadingly.]")
		end
	end

	if (screenID == "information_one") then
		if (not CreatureObject(pPlayer):hasSkill("combat_smuggler_underworld_01")) then
			clonedConversation:setCustomDialogText("Tuskens iki katon ora bisa dikalahake. Iku pisanan aku wis weruh Tuskens dadi pinter iki. /n/n[From the gestures you guess the Tuskens surprised them out of nowhere.]")
		end
	end

	if (screenID == "information_two") then
		if (not CreatureObject(pPlayer):hasSkill("combat_smuggler_underworld_01")) then
			clonedConversation:setCustomDialogText("Padha dipeksa kabeh supaya bisa nyambut gawe, kanggo mbenerake bunker iki. Dheweke ora mangan utawa ngombe, aku bisa urip amarga aku mung siji sing diijini bisa nggarap. /n/n[From the gestures it seems the Tuskens worked these Jawas hard, you guess he only survived because he was working inside.]")
		end
	end

	if (screenID == "complete_quest") then
		if (not CreatureObject(pPlayer):hasSkill("combat_smuggler_underworld_01")) then
			clonedConversation:setCustomDialogText("Maturnuwun, aku bakal bali maneh supaya dheweke ngerti aku aman. /n/n[The Jawa looks grateful as he takes off.]")
		end
		QuestManager.completeQuest(pPlayer, QuestManager.quests.JAWA_RESCUE_OP_TASK)
		QuestManager.activateQuest(pPlayer, QuestManager.quests.JAWA_RESCUE_OP_RETURN)
	end
	
	if (screenID == "decline") then
		if (not CreatureObject(pPlayer):hasSkill("combat_smuggler_underworld_01")) then
			clonedConversation:setCustomDialogText("Aku bakal ngenteni kene nganti aman. /n/n[The Jawa nods and stays put.]")
		end
	end
	return pConvScreen
end
